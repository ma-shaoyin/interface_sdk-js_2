/*
 * Copyright (c) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License"),
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * The observer will be called by system when an error occurs.
 *
 * @since 9
 * @syscap SystemCapability.Ability.AbilityRuntime.Core
 * @permission N/A
 */
export default class ErrorObserver {
    /**
     * Will be called when the js runtime throws an exception which doesn't caught by user.
     *
     * @since 9
     * @syscap SystemCapability.Ability.AbilityRuntime.Core
     * @param errMsg the message and error stacktrace about the exception.
     * @returns -
     */
    onUnhandledException(errMsg: string): void;

    /**
     * Will be called when the native executions exception.
     *
     * @since 10
     * @syscap SystemCapability.Ability.AbilityRuntime.Core
     * @param errObject the error object about the exception.
     * @returns -
     */
    onException?(errObject: Error): void;
}